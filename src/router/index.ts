import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

import { createRouterGuards } from './router-guards'
import 'nprogress/css/nprogress.css' // 进度条样式

import shared from './modules/shared'
import { errorRoutes, notFound } from './modules/error'
// import common from '@/router/common'
import { App } from 'vue'

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Layout',
    redirect: '/dashboard',
    component: () => import(/* webpackChunkName: "layout" */ '@/layout/index.vue'),
    meta: {
      title: '首页'
    },
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: () =>
          import(/* webpackChunkName: "dashboard" */ '@/views/pages/dashboard/dashboard.vue'),
        meta: {
          title: '控制台',
          icon: 'icon-yibiaopan',
          roles: ['admin', 'user'],
          keepAlive: false
        }
      },
      {
        path: 'orderList',
        name: `orderList`,
        meta: {
          title: '任务列表',
          icon: 'icon-shouye',
          roles: ['admin', 'user'],
          keepAlive: false
        },
        component: () =>
          import(/* webpackChunkName: "order" */ '@/views/pages/order/orderList/index.vue')
      },
      {
        path: '/orderEntry',
        name: `orderEntry`,
        meta: {
          title: '订单录入',
          icon: 'icon-shouye',
          roles: ['admin'],
          keepAlive: false
        },
        component: () =>
          import(/* webpackChunkName: "order" */ '@/views/pages/order/orderEntry/index.vue')
      },
      {
        path: '/storeSetup',
        name: `storeSetup`,
        meta: {
          title: '店铺设置',
          icon: 'icon-shouye',
          roles: ['admin'],
          keepAlive: false
        },
        component: () =>
          import(
            /* webpackChunkName: "store" */ '@/views/pages/storeManagement/storeSetup/index.vue'
          )
      },
      {
        path: '/storeAssociation',
        name: `storeAssociation`,
        meta: {
          title: '店铺关联',
          icon: 'icon-shouye',
          roles: ['admin'],
          keepAlive: false
        },
        component: () =>
          import(
            /* webpackChunkName: "store" */ '@/views/pages/storeManagement/storeAssociation/index.vue'
          )
      },
      {
        path: '/keywords',
        name: 'keywords',
        component: () =>
          import(/* webpackChunkName: "keyWords" */ '@/views/pages/keyWords/index.vue'),
        meta: {
          title: '关键词预警',
          icon: 'icon-tixing-jinggao',
          roles: ['admin']
        }
      },
      {
        path: '/account',
        name: 'account',
        component: () =>
          import(/* webpackChunkName: "account" */ '@/views/pages/account/index.vue'),
        meta: {
          title: '账号管理',
          icon: 'icon-tixing-jinggao',
          roles: ['admin']
        }
      },
      {
        path: 'qrcode',
        name: `qrcode`,
        meta: {
          title: '二维码',
          hidden: true
        },
        component: () =>
          import(/* webpackChunkName: "qrcode" */ '@/views/pages/dashboard/qrcode.vue')
      }
    ]
    // children: [...common]
  },
  ...shared,
  errorRoutes
]

const router = createRouter({
  // process.env.BASE_URL
  history: createWebHashHistory(''),
  routes
})

export function setupRouter(app: App) {
  app.use(router)
  // 创建路由守卫
  createRouterGuards(router)
}
export default router
