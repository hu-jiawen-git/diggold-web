import dashboard from '@/router/modules/dashboard'
import demos from '@/router/modules/demos'
import redirect from '@/router/modules/redirect'
import externaLink from '@/router/modules/externa-link'
import order from '@/router/modules/order'
import keywords from '@/router/modules/keyWords'
export default [
  ...dashboard,
  ...order,
  ...keywords,
  // ...demos,
  // ...externaLink,
  ...redirect
]
// export default [...dashboard]
