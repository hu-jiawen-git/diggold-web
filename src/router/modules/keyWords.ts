import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routes: Array<RouteRecordRaw> = [
  {
    path: 'keywords',
    name: 'keywords',
    component: () => import(/* webpackChunkName: "keyWords" */ '@/views/pages/keyWords/index.vue'),
    meta: {
      title: '关键词预警',
      icon: 'icon-tixing-jinggao'
    }
  }
]

export default routes
