import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'order'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/order',
    name: routeName,
    component: RouterTransition,
    meta: {
      title: '订单管理',
      icon: 'icon-rizhi'
    },
    children: [
      {
        path: 'orderList',
        name: `${routeName}-orderList`,
        meta: {
          title: '订单列表',
          icon: 'icon-shouye'
        },
        component: () =>
          import(
            /* webpackChunkName: "order-orderList" */ '@/views/pages/order/orderList/index.vue'
          )
      },
      {
        path: 'orderEntry',
        name: `${routeName}-orderEntry`,
        meta: {
          title: '订单录入',
          icon: 'icon-shouye'
        },
        component: () =>
          import(
            /* webpackChunkName: "order-orderList" */ '@/views/pages/order/orderEntry/index.vue'
          )
      }
    ]
  }
]

export default routes
