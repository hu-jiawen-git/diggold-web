import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'dashboard'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/dashboard',
    name: routeName,
    component: () =>
      import(/* webpackChunkName: "dashboard-welcome" */ '@/views/pages/dashboard/dashboard.vue'),
    meta: {
      title: '控制台',
      icon: 'icon-yibiaopan'
    }
    // children: [
    //   {
    //     path: 'welcome',
    //     name: `${routeName}-welcome`,
    //     meta: {
    //       title: '首页',
    //       icon: 'icon-shouye'
    //     },
    //     component: () =>
    //       import(
    //         /* webpackChunkName: "dashboard-welcome" */ '@/views/pages/dashboard/dashboard.vue'
    //       )
    //   },
    //   // {
    //   //   path: 'buyerlist',
    //   //   name: `buyerlist`,
    //   //   meta: {
    //   //     title: '买手列表',
    //   //     icon: 'icon-yonghu'
    //   //   },
    //   //   component: () =>
    //   //     import(
    //   //       /* webpackChunkName: "dashboard-buyerlist" */ '@/views/pages/dashboard/buyer/index.vue'
    //   //     )
    //   // },
    //   {
    //     path: 'qrcode',
    //     name: `qrcode`,
    //     meta: {
    //       title: '二维码',
    //       hidden: true
    //     },
    //     component: () =>
    //       import(/* webpackChunkName: "dashboard-qrcode" */ '@/views/pages/dashboard/qrcode.vue')
    //   }
    // ]
  }
]

export default routes
