import { useCreateModal } from '@/hooks'
import AddModal from './add-modal.vue'
import { delKeyWords } from '@/api/buyer'
export const columns: TableColumn[] = [
  // 关键词列表
  {
    title: '店铺',
    dataIndex: 'storeName',
    slots: {
      customRender: 'storeName'
    },
    slotsType: 'format',
    slotsFunc: (val) => `${!val ? '所有店铺' : val}` // 格式化状态
  },
  {
    title: '地址关键词',
    dataIndex: 'keywords'
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 200,
    slots: {
      customRender: 'action'
    },
    actions: [
      {
        type: 'button', // 控制类型，默认为a,可选： select | button | text
        text: '编辑',
        permission: {},
        props: {
          type: 'warning' // 按钮类型
        },
        func: ({ record }, callback) =>
          useCreateModal(AddModal, {
            // 点击删除的回调
            fields: record,
            callback
          })
      },
      {
        type: 'popconfirm', // 控制类型，默认为a,可选： select | button | text
        text: '删除',
        props: {
          type: 'danger' // 按钮类型
        },
        permission: {},
        func: async ({ record }, refreshTableData) =>
          await delKeyWords(record.id).then(() => refreshTableData())
      }
    ]
  }
]
