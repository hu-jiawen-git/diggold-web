export const columns: TableColumn[] = [
  // 店铺列表
  {
    title: '商家ID',
    dataIndex: 'store'
  },
  {
    title: '录入时间',
    dataIndex: 'loggingTime'
  },
  {
    title: '订单Id',
    dataIndex: 'orderId'
  },
  {
    title: '淘宝订单号',
    dataIndex: 'taobaoOrderNo'
  },
  {
    title: '旺旺号',
    dataIndex: 'wangwang'
  },
  {
    title: '收货人姓名',
    dataIndex: 'consigneeName'
  },
  {
    title: '收货人手机号',
    dataIndex: 'consigneeTelNo'
  },
  {
    title: '关键词',
    dataIndex: 'keyword'
  },
  {
    title: '店铺名称',
    dataIndex: 'storeName'
  },
  {
    title: '支付宝账号',
    dataIndex: 'account'
  },
  {
    title: '本金',
    dataIndex: 'principal',
    slots: {
      customRender: 'principal'
    },
    slotsType: 'format',
    slotsFunc: (val) => `￥${val / 100}`
  },
  {
    title: '佣金',
    dataIndex: 'commission',
    slots: {
      customRender: 'commission'
    },
    slotsType: 'format',
    slotsFunc: (val) => `￥${val / 100}`
  }
]
