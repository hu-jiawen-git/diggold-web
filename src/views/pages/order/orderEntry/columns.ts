import { createVNode } from 'vue'
import { useCreateModal } from '@/hooks'
import AddModal from './add-modal.vue'
import { delOrder } from '@/api/buyer'
export const columns: TableColumn[] = [
  // 订单录入列表
  {
    title: '订单ID',
    dataIndex: 'id'
  },
  {
    title: '链接',
    dataIndex: 'url',
    ellipsis: true,
    width: 200
  },
  {
    title: '主图',
    dataIndex: 'orderPicture',
    slots: {
      customRender: 'orderPicture'
    },
    width: 100,
    slotsType: 'component',
    slotsFunc: (row) => createVNode('img', { src: row.orderPicture, width: 80, height: 80 }) // 动态创建图标
  },
  // {
  //   title: '关键词',
  //   dataIndex: 'keyword',
  //   ellipsis: true
  // },
  // {
  //   title: '时间段',
  //   dataIndex: 'timeRange'
  // },
  // {
  //   title: '刷单本金',
  //   dataIndex: 'principal'
  // },
  // {
  //   title: '做单次数',
  //   dataIndex: 'tasks'
  // },
  {
    title: '做单要求',
    dataIndex: 'demand'
  },
  // {
  //   title: '订单状态',
  //   dataIndex: 'status',
  //   slots: {
  //     customRender: 'status'
  //   },
  //   slotsType: 'format',
  //   slotsFunc: (val) => (!val ? '已领取' : val === 1 ? '已领取' : '未领取')
  // },
  {
    title: '操作',
    dataIndex: 'action',
    slots: {
      customRender: 'action'
    },
    actions: [
      {
        type: 'button', // 控制类型，默认为a,可选： select | button | text
        text: '编辑',
        permission: {
          // 权限
          // action: 'update',
          // effect: 'disabled'
        },
        props: {
          type: 'warning' // 按钮类型
        },
        func: ({ record }, callback) =>
          useCreateModal(AddModal, {
            // 点击删除的回调
            fields: record,
            callback
          })
      },
      {
        type: 'popconfirm', // 控制类型，默认为a,可选： select | button | text
        text: '删除',
        permission: {
          // // 权限
          // action: 'delete',
          // effect: 'disabled'
        },

        props: {
          type: 'danger'
        },
        func: async ({ record }, refreshTableData) =>
          await delOrder(record.id).then(() => refreshTableData())
      }
    ]
  }
]
