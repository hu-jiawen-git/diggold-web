import { useFormModal } from '@/hooks/useFormModal'
import { getFormSchema } from './form-schema'
import { updateUser, delUser } from '@/api/buyer'
export const columns: TableColumn[] = [
  // 账号列表
  {
    title: '用户名',
    dataIndex: 'name'
  },
  {
    title: '所属角色',
    dataIndex: 'role',
    slots: {
      customRender: 'role'
    },
    slotsType: 'format',
    slotsFunc: (role) => (role === 0 ? '管理员' : '客服')
  },
  // {
  //   title: '创建时间',
  //   dataIndex: 'createdAt',
  //   slots: {
  //     customRender: 'createdAt'
  //   },
  //   slotsType: 'format',
  //   slotsFunc: (val) => formatDate(val)
  // },
  // {
  //   title: '最后更新时间',
  //   dataIndex: 'updatedAt',
  //   slots: {
  //     customRender: 'updatedAt'
  //   },
  //   slotsType: 'format',
  //   slotsFunc: (val) => formatDate(val)
  // },
  {
    title: '操作',
    dataIndex: 'action',
    width: 200,
    slots: {
      customRender: 'action'
    },
    actions: [
      {
        type: 'button', // 控制类型，默认为a,可选： select | button | text
        text: '编辑',
        permission: {
          // 权限
          // action: 'update',
          // effect: 'disabled'
        },

        props: {
          type: 'warning'
        },
        func: ({ record }, refreshTableData) =>
          useFormModal({
            title: '编辑账号',
            fields: { ...record, role: record.role + '' },
            // hiddenFields: ['passwd'],
            formSchema: getFormSchema(),
            handleOk: async (modelRef, state) => {
              const { name, passwd, role } = modelRef
              const params = {
                id: record.id,
                name,
                passwd,
                role: Number(role)
              }
              return await updateUser(params).then(() => refreshTableData())
            }
          })
      },
      {
        type: 'popconfirm', // 控制类型，默认为a,可选： select | button | text
        text: '删除',
        permission: {
          // // 权限
          // action: 'delete',
          // effect: 'disabled'
        },

        props: {
          type: 'danger'
        },
        func: async ({ record }, refreshTableData) =>
          await delUser(record.id).then(() => refreshTableData())
      }
    ]
  }
]
