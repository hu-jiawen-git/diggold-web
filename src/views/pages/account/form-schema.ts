// import { getAdminRole } from '@/api/system/role'

// 与vue2的里面的data一样，函数返回新对象防止多处共用同一对象,造成数据混乱
export const getFormSchema = (): FormSchema => ({
  formItem: [
    {
      type: 'input',
      label: '用户名',
      field: 'name',
      value: '',
      props: {
        placeholder: '请输入用户名'
      },
      rules: [
        {
          required: true,
          message: '用户名不能为空'
        }
      ]
    },
    {
      type: 'input',
      label: '密码',
      field: 'passwd',
      hidden: false, // 是否隐藏
      value: '',
      props: {
        type: 'password',
        placeholder: '请输入密码'
      },
      rules: [
        {
          required: true,
          message: '密码不能为空'
        }
      ]
    },
    {
      type: 'radio',
      label: '角色',
      field: 'role',
      value: '',
      options: [],
      loading: true,
      rules: [
        {
          required: true,
          message: '请选择角色'
        }
      ],
      asyncOptions: async () => {
        // 获取角色列表
        const data = [
          {
            label: '管理员',
            value: '0'
          },
          {
            label: '客服',
            value: '1'
          }
        ]
        return data
      }
    }
  ]
})
