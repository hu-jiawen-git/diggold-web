export const columns: TableColumn[] = [
  // 账号列表
  {
    title: 'ID',
    dataIndex: 'id',
    width: 50
  },
  {
    title: '登录时间',
    dataIndex: 'loginTime'
  },
  {
    title: '旺旺号',
    dataIndex: 'wangwang'
  },
  {
    title: '手机号',
    dataIndex: 'telNo'
  },
  {
    title: '定位地址',
    dataIndex: 'address',
    ellipsis: true
  },
  {
    title: '微信昵称',
    dataIndex: 'nickName',
    slots: {
      customRender: 'nickName'
    },
    slotsType: 'format',
    slotsFunc: (val) => `${val ? val : '-'}`
  },
  {
    title: 'QQ',
    dataIndex: 'qq',
    slots: {
      customRender: 'qq'
    },
    slotsType: 'format',
    slotsFunc: (val) => `${val ? val : '-'}`
  },
  {
    title: '创建IP',
    dataIndex: 'ipAddr'
  },
  {
    title: '距离',
    dataIndex: 'distance',
    slots: {
      customRender: 'distance'
    },
    slotsType: 'format',
    slotsFunc: (val) => `${val === 0 ? '-' : val + '米'}` // 格式化状态
  },
  {
    title: '经纬度',
    dataIndex: 'LongitudeLatitude',
    slots: { customRender: 'LongitudeLatitude' }
  },
  {
    title: '所属店铺',
    dataIndex: 'storeName'
  },
  {
    title: '所属员工',
    dataIndex: 'employeeName'
  },
  {
    title: '状态',
    dataIndex: 'status',
    slots: {
      customRender: 'status'
    }
  },
  {
    title: '操作',
    dataIndex: 'action',
    fixed: 'right',
    slots: {
      customRender: 'action'
    },
    actions: []
  }
]
