import { useCreateModal } from '@/hooks'
import AddModal from './add-modal.vue'
import router from '@/router'
import { delStore } from '@/api/buyer'
export const columns: TableColumn[] = [
  // 订单列表
  {
    title: '商家ID',
    dataIndex: 'id'
  },
  {
    title: '店铺名称',
    dataIndex: 'name'
  },
  {
    title: '复购时间',
    dataIndex: 'nextBuyTime',
    slots: {
      customRender: 'nextBuyTime'
    },
    slotsType: 'format',
    slotsFunc: (val) => `${val === 0 ? '无限制' : `${val}天`}`
  },
  {
    title: '预警距离',
    dataIndex: 'distance',
    slots: {
      customRender: 'distance'
    },
    slotsType: 'format',
    slotsFunc: (val) => `${val}米`
  },
  {
    title: '返还佣金',
    dataIndex: 'commission',
    slots: {
      customRender: 'commission'
    },
    slotsType: 'format',
    slotsFunc: (val) => `￥${val / 100}`
  },
  {
    title: '最大领取任务数',
    dataIndex: 'maxTaskCount'
  },
  {
    title: '最大返款金额',
    dataIndex: 'maxRefundAmount',
    slots: {
      customRender: 'maxRefundAmount'
    },
    slotsType: 'format',
    slotsFunc: (val) => `￥${val / 100}`
  },
  {
    title: '创建时间',
    dataIndex: 'createTime'
  },
  {
    title: '操作',
    dataIndex: 'action',
    slots: {
      customRender: 'action'
    },
    actions: [
      {
        type: 'button', // 控制类型，默认为a,可选： select | button | text
        text: '编辑',
        permission: {
          // 权限
          // action: 'update',
          // effect: 'disabled'
        },
        props: {
          type: 'warning' // 按钮类型
        },
        func: ({ record }, callback) =>
          useCreateModal(AddModal, {
            // 点击删除的回调
            fields: record,
            callback
          })
      },
      // {
      //   type: 'button', // 控制类型，默认为a,可选： select | button | text
      //   text: '二维码',
      //   permission: {
      //     // 权限
      //     // action: 'update',
      //     // effect: 'disabled'
      //   },
      //   func: ({ record }, callback) => {
      //     router.push(`/qrcode?url=${record.url}`)
      //   }
      // },
      {
        type: 'popconfirm', // 控制类型，默认为a,可选： select | button | text
        text: '删除',
        permission: {
          // // 权限
          // action: 'delete',
          // effect: 'disabled'
        },

        props: {
          type: 'danger'
        },
        func: async ({ record }, refreshTableData) =>
          await delStore(record.id).then(() => refreshTableData())
      }
    ]
  }
]
