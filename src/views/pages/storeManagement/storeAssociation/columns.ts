import { useCreateModal } from '@/hooks'
import AddModal from './add-modal.vue'
import { delRelatedstore } from '@/api/buyer'
export const columns: TableColumn[] = [
  // 订单列表
  {
    title: '关联店铺ID',
    dataIndex: 'relatedStoreIds'
  },
  {
    title: '关联店铺名称',
    dataIndex: 'relatedStoreNames'
  },
  {
    title: '复购限制',
    dataIndex: 'nextBuyTime',
    slots: {
      customRender: 'nextBuyTime'
    },
    slotsType: 'format',
    slotsFunc: (val) => `${val}天`
  },
  {
    title: '创建时间',
    dataIndex: 'createTime'
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 200,
    slots: {
      customRender: 'action'
    },
    actions: [
      {
        type: 'button', // 控制类型，默认为a,可选： select | button | text
        text: '编辑',
        permission: {},
        props: {
          type: 'warning' // 按钮类型
        },
        func: ({ record }, callback) =>
          useCreateModal(AddModal, {
            // 点击编辑的回调
            fields: record,
            callback
          })
      },
      {
        type: 'popconfirm', // 控制类型，默认为a,可选： select | button | text
        text: '删除',
        permission: {
          // // 权限
          // action: 'delete',
          // effect: 'disabled'
        },

        props: {
          type: 'danger'
        },
        func: async ({ record }, refreshTableData) =>
          await delRelatedstore(record.id).then(() => refreshTableData())
      }
      // {
      //   type: 'button', // 控制类型，默认为a,可选： select | button | text
      //   text: '二维码',
      //   permission: {},

      //   func: ({ record }, callback) => {}
      // }
    ]
  }
]
