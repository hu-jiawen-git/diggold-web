import type { App } from 'vue'

import { AButton } from '@/components/button/index'

import {
  Modal,
  Table,
  Menu,
  Input,
  Form,
  Card,
  Checkbox,
  Radio,
  Select,
  DatePicker,
  InputNumber,
  Space
} from 'ant-design-vue'

import 'ant-design-vue/dist/antd.css'

export function setupAntd(app: App<Element>) {
  app.component('AButton', AButton)
  app.component('ASelect', Select)
  app
    .use(Form)
    .use(Input)
    .use(Modal)
    .use(Table)
    .use(Menu)
    .use(Card)
    .use(Checkbox)
    .use(Radio)
    .use(Select)
    .use(DatePicker)
    .use(InputNumber)
    .use(Space)
}
