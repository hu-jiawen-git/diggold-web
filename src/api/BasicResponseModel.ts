export interface BasicResponseModel<T = any> {
  status: string
  result: T
}
export interface BasicPageParams {
  pageNumber: number
  pageSize: number
  total: number
}
