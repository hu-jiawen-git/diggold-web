import http from '@/utils/http/axios'
import { RequestEnum } from '@/enums/httpEnum'
import { ContentTypeEnum } from '@/enums/httpEnum'
/**
 * 创建用户
 * @param params
 */
export function creatUser(params?: object) {
  return http.request(
    {
      url: '/user/create',
      method: RequestEnum.POST,
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '新建成功'
    }
  )
}
/**
 * 获取用户信息
 * @param params
 */
export function getUser(params) {
  return http.request({
    url: '/user/' + `${params.id}`,
    method: RequestEnum.GET,
    params
  })
}
/**
 * 更新用户
 * @param params
 */
export function updateUser(params) {
  return http.request(
    {
      url: '/user/update',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '修改成功'
    }
  )
}
/**
 * 查询用户列表
 * @param params
 */
export function getUserList(params?: object) {
  return http.request({
    url: '/user/list',
    method: 'POST',
    params
  })
}
/**
 * 删除用户
 * @param params
 */
export function delUser(id) {
  return http.request(
    {
      url: `/user/delete/${id}`,
      method: 'GET'
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '删除成功'
    }
  )
}
/**
 * 新建店铺
 * @param params
 */
export function createStore(params?: object) {
  return http.request(
    {
      url: '/store/create',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '新建店铺成功'
    }
  )
}
/**
 * 编辑店铺
 * @param params
 */
export function updateStore(params) {
  return http.request(
    {
      url: '/store/update',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '编辑店铺成功'
    }
  )
}
/**
 * 店铺列表
 * @param params
 */
export function getStoreList(params?: object) {
  return http.request({
    url: '/store/list',
    method: 'POST',
    params
  })
}
/**
 * 删除店铺
 * @param params
 */
export function delStore(id) {
  return http.request(
    {
      url: `/store/delete/${id}`,
      method: 'GET'
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '删除成功'
    }
  )
}
/**
 * 获取所有店铺列表
 * @param params
 */
export function getAllStore(params?: object) {
  return http.request({
    url: '/store/list',
    method: RequestEnum.POST,
    params: {
      store: {
        name: null,
        url: null,
        nextBuyTime: null,
        distance: null
      },
      limit: 1000,
      offset: 0
    }
  })
}
/**
 * 店铺关联列表
 * @param params
 */
export function getRelatedStoreList(params?: object) {
  return http.request({
    url: '/relatedstore/list',
    method: 'POST',
    params
  })
}
/**
 * 新建店铺
 * @param params
 */
export function createRelatedstore(params?: object) {
  return http.request(
    {
      url: '/relatedstore/create',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '新建成功'
    }
  )
}
/**
 * 编辑关联店铺
 * @param params
 */
export function updateRelatedStore(params) {
  return http.request(
    {
      url: '/relatedstore/update',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '编辑成功'
    }
  )
}
/**
 * 删除关联
 * @param params
 */
export function delRelatedstore(id) {
  return http.request(
    {
      url: `/relatedstore/delete/${id}`,
      method: 'GET'
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '删除成功'
    }
  )
}
// 订单录入
/**
 * 订单录入列表
 * @param params
 */
export function getOrderList(params?: object) {
  return http.request({
    url: '/storeorder/list',
    method: 'POST',
    params
  })
}
/**
 * 创建录入订单
 * @param params
 */
export function createOrder(params?: object) {
  return http.request(
    {
      url: '/storeorder/create',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '新建订单成功'
    }
  )
}
/**
 * 更新录入订单
 * @param params
 */
export function updateOrder(params?: object) {
  return http.request(
    {
      url: '/storeorder/update',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '编辑成功'
    }
  )
}
/**
 * 删除录入订单
 * @param params
 */
export function delOrder(id) {
  return http.request(
    {
      url: `/storeorder/delete/${id}`,
      method: 'GET'
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '删除成功'
    }
  )
}
/**
 * 导出模板
 * @param params
 */
export function download() {
  return http.request({
    url: `/storeorder/download?filename=template.xlsx`,
    method: 'GET'
  })
}
// 订单列表模块
/**
 * 任务列表
 * @param params
 */
export function getTaskList(params?: object) {
  return http.request({
    url: '/task/list',
    method: 'POST',
    params
  })
}
/**
 * 任务列表导出
 * @param params
 */
export function taskDownload(params?: object) {
  return http.request({
    url: '/task/download',
    method: 'POST',
    params
  })
}
// 买手列表
/**
 * 订单列表
 * @param params
 */
export function getBuyerList(params?: object) {
  return http.request({
    url: '/buyer/list',
    method: 'POST',
    params
  })
}
/**
 * 订单列表
 * @param params
 */
export function updateBuyer(params?: object) {
  return http.request(
    {
      url: '/buyer/update',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '修改成功'
    }
  )
}
/**
 * 任务分配
 * @param params
 */
export function orderAssign(params?: object) {
  return http.request({
    url: '/order/assign',
    method: 'POST',
    params
  })
}
/**
 * 图片上传
 * @param params
 */
export function upload(params?: object) {
  return http.request({
    url: '/user/upload/profilephoto',
    method: 'POST',
    params,
    headers: {
      'Content-type': ContentTypeEnum.FORM_DATA
    }
  })
}
/**
 * 任务确认完成
 * @param params
 */
export function orderComplete(params?: object) {
  return http.request({
    url: '/order/complete',
    method: 'POST',
    params
  })
}
/**
 * 任务退款重做
 * @param params
 */
export function orderRedo(params?: object) {
  return http.request({
    url: '/order/redo',
    method: 'POST',
    params
  })
}
/**
 * 获取订单详情
 * @param params
 */
export function getOrderDetail(params) {
  return http.request({
    url: `/order/${params.id}`,
    method: 'get',
    params
  })
}
/**
 * 任务确认
 * @param params
 */
export function createTask(params?: object) {
  return http.request({
    url: '/task/create',
    method: 'POST',
    params
  })
}
// 关键词
/**
 * 关键词列表
 * @param params
 */
export function getKeyWordsList(params?: object) {
  return http.request({
    url: '/keywords/list',
    method: 'POST',
    params
  })
}
/**
 * 创建关键词
 * @param params
 */
export function createKeyWords(params?: object) {
  return http.request(
    {
      url: '/keywords/create',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '新建关键词成功'
    }
  )
}
/**
 * 更新关键词
 * @param params
 */
export function updateKeyWords(params?: object) {
  return http.request(
    {
      url: '/keywords/update',
      method: 'POST',
      params
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '编辑关键词成功'
    }
  )
}
/**
 * 删除关键词
 * @param params
 */
export function delKeyWords(id?: object) {
  return http.request({
    url: `/keywords/delete/${id}`,
    method: 'get'
  })
}
/**
 * 返款
 * @param params
 */
export function transferAccount(params?: object) {
  return http.request({
    url: '/buyer/account/transfer',
    method: 'POST',
    params
  })
}
/**
 * 获取买手附近的人
 * @param params
 */
export function queryListnear(id) {
  return http.request({
    url: `/buyer/listnear/${id}`,
    method: 'get'
  })
}
/**
 * 获取买手附近的人
 * @param params
 */
export function querybuyerListall() {
  return http.request({
    url: `/buyer/listall`,
    method: 'get'
  })
}
/**
 * 获取所有客服
 * @param params
 */
export function queryListallemployee() {
  return http.request({
    url: `/user/listallemployee`,
    method: 'get'
  })
}

/**
 * 查询今日剩余订单数
 * @param params
 */
export function queryLeftordercount() {
  return http.request({
    url: `/order/leftordercount`,
    method: 'get'
  })
}
/**
 * 删除买手
 * @param params
 */
export function deleteBuyer(id?: object) {
  return http.request(
    {
      url: `/buyer/delete/${id}`,
      method: 'get'
    },
    {
      isShowErrorMessage: true, // 是否显示错误提示信息
      successMessageText: '删除成功'
    }
  )
}
