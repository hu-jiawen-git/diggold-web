import http from '@/utils/http/axios'
import { BasicResponseModel } from '@/api/BasicResponseModel'
import { LoginParams, LoginResultModel } from './model/userModel'
import { ContentTypeEnum } from '@/enums/httpEnum'
enum Api {
  login = '/user/login',
  logout = '/login/logout',
  createCode = '/user/createCode',
  getIp = '/api/getIp'
}

/**
 * @description: 获取用户信息
 */
export function getUserInfo() {
  return http.request(
    {
      url: Api.login,
      method: 'GET'
    },
    {
      isTransformRequestResult: false
    }
  )
}
/**
 * @description: 获取登录验证码
 */
export function createCode() {
  return http.request(
    {
      url: Api.createCode + `?number=${Math.random()}`,
      method: 'GET'
    },
    {
      isTransformRequestResult: false
    }
  )
}
/**
 * @description: 用户登录
 */
export function login(params: LoginParams) {
  return http.request<BasicResponseModel<LoginResultModel>>(
    {
      url: Api.login,
      method: 'POST',
      params,
      headers: {
        'Content-Type': ContentTypeEnum.JSON
      }
    },
    {
      isTransformRequestResult: false,
      isParseToJson: true
    }
  )
}

/**
 * @description: 用户修改密码
 */
export function changePassword(params, uid) {
  return http.request(
    {
      url: `/user/u${uid}/changepw`,
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

/**
 * @description: 用户登出
 */
export function logout(params) {
  return http.request({
    url: Api.logout,
    method: 'POST',
    params
  })
}
/**
 * @description: 搜狐获取IP
 */
export function getIp() {
  return http.request({
    url: Api.getIp,
    method: 'get'
  })
}
